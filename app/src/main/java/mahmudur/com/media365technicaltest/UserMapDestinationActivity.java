package mahmudur.com.media365technicaltest;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserMapDestinationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    TextView txtcurrentAddress;
    TextView textViewDestinationAddress;
    ImageView imageViewCenterHud;
    Button buttonDestination;
    Button buttonDone;

    LatLng pickerLatLng;
    LatLng destinationLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_map_destination);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        txtcurrentAddress = (TextView) findViewById(R.id.txt_Map_CurrentAddress);
        textViewDestinationAddress = (TextView) findViewById(R.id.txt_Map_DestinationAddress);
        imageViewCenterHud = (ImageView) findViewById(R.id.img_centerhud);

        buttonDestination = (Button) findViewById(R.id.btn_SelectDestination);
        buttonDone = (Button) findViewById(R.id.btn_DoneMap);
        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }

        buttonDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LatLng destLatLong = mGoogleMap.getCameraPosition().target;
                destinationLatLng = destLatLong;
                mGoogleMap.addMarker(new MarkerOptions().position(destLatLong).title("Custom location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                getCompleteAddressString(destLatLong.latitude, destLatLong.longitude, 2);
                imageViewCenterHud.setVisibility(View.GONE);
                buttonDestination.setEnabled(false);
                buttonDestination.setVisibility(View.GONE);
                buttonDone.setVisibility(View.VISIBLE);
                if (mGoogleApiClient != null) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, UserMapDestinationActivity.this);
                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Location startPoint = new Location("locationA");
                startPoint.setLatitude(pickerLatLng.latitude);
                startPoint.setLongitude(pickerLatLng.longitude);

                Location endPoint = new Location("locationA");
                endPoint.setLatitude(destinationLatLng.latitude);
                endPoint.setLongitude(destinationLatLng.longitude);

                double distance = startPoint.distanceTo(endPoint);
                double disInKM = (distance / 1000000);

                Toast.makeText(getApplicationContext(), "Distance approximately: " + disInKM + " KM", Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(15000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Pickup");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        getCompleteAddressString(location.getLatitude(), location.getLongitude(), 1);
        pickerLatLng = new LatLng(location.getLatitude(), location.getLatitude());
    }

    private void getCompleteAddressString(double lat, double lon, final int setpos) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&keys=AIzaSyAgkrLprQseuz2IobQ6TmPWpiByfBj609s",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject ersObj = new JSONObject(response);
                            JSONArray addressArr = ersObj.getJSONArray("results");
                            JSONObject addressObj = addressArr.getJSONObject(0);
                            String useraddress = addressObj.getString("formatted_address");
                            String search = "Dhaka";
                            if (setpos == 1) {
                                if (useraddress.toLowerCase().indexOf(search.toLowerCase()) != -1) {
                                    txtcurrentAddress.setText(useraddress);
                                } else {
                                    txtcurrentAddress.setText("Out side dhaka not allow.");
                                }

                            } else if (setpos == 2) {
                                if (useraddress.toLowerCase().indexOf(search.toLowerCase()) != -1) {
                                    textViewDestinationAddress.setText(useraddress);
                                } else {
                                    textViewDestinationAddress.setText("Out side dhaka not allow.");
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(UserMapDestinationActivity.this, "Internet Problem Occour", Toast.LENGTH_LONG).show();
                    }
                }) {


        };
        RequestQueue requestQueue = Volley.newRequestQueue(UserMapDestinationActivity.this);
        requestQueue.add(stringRequest);
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(UserMapDestinationActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
}
