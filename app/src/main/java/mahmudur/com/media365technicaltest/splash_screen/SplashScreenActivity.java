package mahmudur.com.media365technicaltest.splash_screen;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import mahmudur.com.media365technicaltest.MainActivity;
import mahmudur.com.media365technicaltest.R;
import mahmudur.com.media365technicaltest.UserMapDestinationActivity;

public class SplashScreenActivity extends AppCompatActivity implements Animation.AnimationListener {

    private ImageView imageViewMainLogo;
    Animation animZoomOut;
    long Delay = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.statusbar_color));
        }


        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.INTERNET
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                loadSplash();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }

        }).check();

    }

    private void loadSplash() {
        imageViewMainLogo = (ImageView) findViewById(R.id.img_Splash_MainLogo);
        animZoomOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
        animZoomOut.setAnimationListener(this);

        imageViewMainLogo.setAnimation(animZoomOut);

        Timer RunSplash = new Timer();
        TimerTask showSplash = new TimerTask() {
            @Override
            public void run() {
                finish();
                Intent goMainActivity = new Intent(getApplicationContext(), UserMapDestinationActivity.class);
                ActivityOptions options = ActivityOptions.makeCustomAnimation(SplashScreenActivity.this, R.anim.fade_in, R.anim.fade_out);
                goMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                goMainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(goMainActivity, options.toBundle());
            }
        };
        RunSplash.schedule(showSplash, Delay);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
